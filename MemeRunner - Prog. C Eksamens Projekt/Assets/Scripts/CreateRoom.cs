﻿using System;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class CreateRoom : Photon.PunBehaviour, IPunCallbacks
{
    [SerializeField]
    private byte maxPlayersPerRoom;
    [SerializeField]
    private byte minPlayersPerRoom;

    [SerializeField] private Dropdown playerNum;
    [SerializeField]
    private Toggle toggleVO;
    [SerializeField]
    private Toggle toggleP;
    [SerializeField]
    private InputField roomNI;
    [SerializeField]
    private InputField roomPI;

    private void Awake()
    {
        List<string> toAdd = new List<string>();
        for (int i = minPlayersPerRoom; i <= maxPlayersPerRoom; i++)
        {
            toAdd.Add(i + " Players");
        }
        playerNum.AddOptions(toAdd);
    }

    public void Create()
    {
        if (roomNI.text.Trim() == string.Empty)
        {
            ManageMenu.DisplayMessage("Please enter a room name with atleast one alphanumeric character.", 3);
            return;
        }
        RoomOptions rOptions = new RoomOptions();
        rOptions.IsVisible = toggleVO.isOn;

        byte mPlayers = byte.Parse(playerNum.value.ToString());
        mPlayers += minPlayersPerRoom;

        rOptions.MaxPlayers = mPlayers;

        rOptions.CustomRoomPropertiesForLobby = new[] {"passEnabled", "pass"};
        string[] chatList = { "Welcome to the chat! Max players in this room is " + mPlayers + "."};
        Hashtable h = new Hashtable(3) {{"passEnabled", toggleP.isOn}, {"pass", roomPI.text}, { "chatArray", chatList} };
        rOptions.CustomRoomProperties = h;

        PhotonNetwork.CreateRoom(roomNI.text, rOptions, null);
    }
}
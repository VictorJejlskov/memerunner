﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ManageMenu : Photon.PunBehaviour, IPunCallbacks
{
    [SerializeField] private GameObject startMenu;
    [SerializeField] private GameObject createMenu;
    [SerializeField] private GameObject joinMenu;
    [SerializeField] private GameObject dispObj;

    [SerializeField] InputField userNameInput;

    public static string playerNamePrefKey;
    public static string version = "Alpha 0.0.1";

    [SerializeField] private PhotonLogLevel logLevel;

    private void Awake()
    {
        PhotonNetwork.autoJoinLobby = true;
        PhotonNetwork.automaticallySyncScene = true;
        PhotonNetwork.logLevel = logLevel;
        string defaultName = "";
        if (userNameInput != null)
        {
            if (PlayerPrefs.HasKey(playerNamePrefKey))
            {
                defaultName = PlayerPrefs.GetString(playerNamePrefKey);
                userNameInput.text = defaultName;
            }
            PhotonNetwork.playerName = defaultName;
        }
        if (!PhotonNetwork.connected)
        {
            PhotonNetwork.ConnectUsingSettings(version);
        }
    }

    public void SetPlayerName(string name)
    {
        PhotonNetwork.playerName = name + " ";
        PlayerPrefs.SetString(playerNamePrefKey, name);
    }

    public void Back()
    {
        if (startMenu.activeSelf == false)
        {
            createMenu.SetActive(false);
            joinMenu.SetActive(false);
            startMenu.SetActive(true);
        }
        else
        {
            Application.Quit();
        }
    }
    
    public static void DisplayMessage(string msg, byte s)
    {
        //GameObject display = new GameObject();
        //display.AddComponent<Text>();
        //display.transform.SetParent(GameObject.Find("Canvas").transform);
        //display.AddComponent<RectTransform>();
        //display.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        //display.GetComponent<RectTransform>().localPosition = new Vector3(0,0,0);

        //Text dText = display.GetComponent<Text>();
        //dText.font = U
        //dText.text = "Message:\n" + msg;
        //dText.fontSize = 20;
        //dText.raycastTarget = false;
        ////Destroy(display, s);
        Debug.Log("DM: " + msg);
    }

    void IPunCallbacks.OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        foreach (object obj in codeAndMsg)
        {
            DisplayMessage((string)obj, 0);
        }
    }

    void IPunCallbacks.OnPhotonCreateRoomFailed(object[] codeAndMsg)
    {
        foreach (object obj in codeAndMsg)
        {
            DisplayMessage((string) obj, 0);
        }
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("DemoAnimator/Launch: OnConnectedToMaster() was called by PUN");
    }

    public override void OnDisconnectedFromPhoton()
    {
        Debug.LogWarning("DemoAnimator/Launch: OnDisconnectedFromPhoton() was called by PUN");
        SceneManager.LoadScene("Start");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("DemoAnimator/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
        SceneManager.LoadScene("Room");
    }

    
}

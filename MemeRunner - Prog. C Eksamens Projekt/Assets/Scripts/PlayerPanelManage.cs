﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerPanelManage : MonoBehaviour
{
    [SerializeField] public Text playerScore;
    [SerializeField] public Text playerInfo;
    [SerializeField] public Image charIcon;
    [SerializeField] public Toggle ready;
}

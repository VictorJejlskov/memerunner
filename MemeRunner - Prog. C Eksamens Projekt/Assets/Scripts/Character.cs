﻿using UnityEngine;

public class Character : MonoBehaviour
{

    public readonly string name;
    public readonly byte ID;
    public readonly Sprite icon;
    public readonly int speed;
    public readonly int jumpHeight;
    //Add other stats

    //Add link to animation set

    public Character(string cname, byte cID, int cspeed,int cjumpHeight)
    {
        name = cname;
        ID = cID;
        icon = Resources.Load<Sprite>("CharacterIcons/" + name); ;
        speed = cspeed;
        jumpHeight = cjumpHeight;
    }
}

﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class JoinRoom : MonoBehaviour
{

    [SerializeField]
    private InputField joinRI;
    [SerializeField]
    private InputField joinPI;

    public void Connect()
    {
        if (PhotonNetwork.connected)
        {
            if (string.IsNullOrEmpty(joinRI.text))
            {
                ManageMenu.DisplayMessage("Please enter a room name", 3);
                return;
            }
            if (CheckPassword(joinRI.text))
            {
                ManageMenu.DisplayMessage("Joined room: " + joinRI.text, 3);
                PhotonNetwork.JoinRoom(joinRI.text);
            }
            else
            {
                ManageMenu.DisplayMessage("Failed to join room '" + joinRI.text + "'", 3);
            }
        }
        else
        {
            PhotonNetwork.ConnectUsingSettings(ManageMenu.version);
        }
    }

    private bool CheckPassword(string room)
    {
        if (PhotonNetwork.insideLobby)
        {
            if (PhotonNetwork.GetRoomList() == null || PhotonNetwork.GetRoomList().Length == 0) return false;
            foreach (RoomInfo existingRoom in PhotonNetwork.GetRoomList())
            {
                Hashtable h = existingRoom.CustomProperties;
                if (existingRoom.Name != room) continue;
                if ((bool) h["passEnabled"])
                {
                    string s = (string)h["pass"];
                    if (string.IsNullOrEmpty(s)) return true;
                    return (s == joinPI.text);
                }
                return true;
            }
        }
        return false;
    }
}

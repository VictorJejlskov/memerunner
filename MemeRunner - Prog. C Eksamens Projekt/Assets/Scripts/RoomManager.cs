﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using System.Linq;

public class RoomManager : Photon.PunBehaviour
{
    [SerializeField] private Text info;
    [SerializeField] private GameObject[] playerPanels;
    [SerializeField] private InputField chatInputField;
    [SerializeField] private Text chatTextField;
    [SerializeField] private Dropdown charDropdown;
    private static byte maxChatLines = 100;
    private List<string> chatArray = new List<string>();

    private static int localPlayer;

    private PhotonPlayer cur;

    private void Start()
    {
        PhotonNetwork.automaticallySyncScene = true;
        Hashtable h = new Hashtable(2) { {"ready", false}, {"character", 0}, {"points", 0} };
        PhotonNetwork.player.SetCustomProperties(h);

        UpdateLocalChat();
        DrawPlayerPanels();

        List<string> charNames = new List<string>();
        foreach (Character c in Database.chars)
        {
            charNames.Add(c.name);
        }
        charDropdown.AddOptions(charNames);
    }

    public void OnReady(Text bText)
    {
        Hashtable h = new Hashtable();
        switch (bText.text)
        {
            case "Ready":
                h = new Hashtable() { { "ready", true } };
                bText.text = "Unready";
                break;
            case "Unready":
                h = new Hashtable() { { "ready", false } };
                bText.text = "Ready";
                break;
        }
        PhotonNetwork.player.SetCustomProperties(h);
        photonView.RPC("UpdatePanels", PhotonTargets.All);
    }

    public void OnCharacterSelect()
    {
        
    }

    [PunRPC]
    void UpdateCharacters()
    {
        for (int i = 0; i < PhotonNetwork.room.PlayerCount; i++)
        {
            PlayerPanelManage pPM = playerPanels[i].GetComponent<PlayerPanelManage>();
            pPM.charIcon.sprite = Database.chars[(int)PhotonNetwork.playerList[i].CustomProperties["character"]].icon;
        }
    }

    [PunRPC]
    void UpdatePanels()
    {
        for (int i = 0; i < PhotonNetwork.room.PlayerCount; i++)
        {
            PlayerPanelManage pPM = playerPanels[i].GetComponent<PlayerPanelManage>();
            pPM.ready.isOn = (bool) PhotonNetwork.playerList[i].CustomProperties["ready"];
        }
    }

    public void OnChatEnter()
    {
        if (Input.GetKeyDown(KeyCode.Return) && !string.IsNullOrEmpty(chatInputField.text.Trim()))
        {
            ChatSend(PhotonNetwork.playerName + ": " + chatInputField.text);
            chatInputField.text = string.Empty;
        }
    }

    private void ChatSend(string send)
    {
        photonView.RPC("ChatReceive", PhotonTargets.MasterClient, send);
    }

    [PunRPC]
    void ChatReceive(string receive)
    {
            if (chatArray.Count >= maxChatLines)
            {
                chatArray.RemoveAt(0);
            }

            chatArray.Add(receive);

            Hashtable h = new Hashtable() { {"chatArray", chatArray.ToArray()} };

            PhotonNetwork.room.SetCustomProperties(h);
            photonView.RPC("UpdateLocalChat", PhotonTargets.All);
    }

    [PunRPC]
    void UpdateLocalChat()
    {
        chatArray = new List<string>();
        foreach (string line in PhotonNetwork.room.CustomProperties["chatArray"] as string[])
        {
            chatArray.Add(line);
        }
        chatTextField.text = string.Empty;
        foreach (string line in chatArray)
        {
            chatTextField.text += line + "\n";
        }
        chatTextField.text = chatTextField.text.TrimEnd();
    }

    private void DrawPlayerPanels()
    {
        info.text = "Room Name: " + PhotonNetwork.room.Name + "\nPlayers: " + PhotonNetwork.room.PlayerCount + "/" + PhotonNetwork.room.MaxPlayers;
        for (int i = 0; i < PhotonNetwork.room.MaxPlayers; i++)
        {
            if (i < PhotonNetwork.room.PlayerCount)
            {
                cur = PhotonNetwork.playerList[i];
                if (cur.ID == PhotonNetwork.player.ID)
                {
                    localPlayer = i;
                }
            }
            playerPanels[i].SetActive(i < PhotonNetwork.room.PlayerCount);
            if (playerPanels[i].activeSelf)
            {
                PlayerPanelManage pPM = playerPanels[i].GetComponent<PlayerPanelManage>();
                pPM.playerInfo.text = "Nickname: " + cur.NickName + "\nID: " + cur.ID;
                
            }
        }
    }

    public void Disconnect()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        if (PhotonNetwork.isMasterClient)
        {
            ChatReceive("SERVER: " + player.NickName + " left the room. There are now " + PhotonNetwork.room.PlayerCount + "/" + PhotonNetwork.room.MaxPlayers + " players in this room.");
        }
        DrawPlayerPanels();
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        if (PhotonNetwork.isMasterClient)
        {
            ChatReceive("SERVER: " + player.NickName + " joined the room. There are now " + PhotonNetwork.room.PlayerCount + "/" + PhotonNetwork.room.MaxPlayers + " players in this room.");
        }
        DrawPlayerPanels();
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("Start");
    }

    public override void OnDisconnectedFromPhoton()
    {
        Debug.LogWarning("DemoAnimator/Launch: OnDisconnectedFromPhoton() was called by PUN");
        SceneManager.LoadScene("Start");
    }
}
 